package daos;

import pacakgesexceptions.NoSQLResultsException;
import models.Customer; 


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

public class CustomerDAO implements Dao<Customer>{
    //private List<TestTable> testTables;
    Connection connection;

    public CustomerDAO(Connection conn) {
        //testTables = new ArrayList<>();
        connection = conn;
    }

    @Override
    public Customer get(int id) throws SQLException, NoSQLResultsException {
        String sql = "Select * FROM customers WHERE customer_id = ? LIMIT 1";
        PreparedStatement pstmt = connection.prepareStatement(sql);
        pstmt.setInt(1, id);

        ResultSet rs = pstmt.executeQuery();
        if(rs.next()) {
        	Customer row = new Customer();
            row.setcustomer_id(rs.getInt("customer_id"));
            row.setname(rs.getString("name"));
            return row;
        } else {
            throw new NoSQLResultsException ("ID: " + id + " not found.");
        }
    }

    @Override
    public List<Customer> getAll() throws SQLException {
        String sql = "SELECT * FROM customers";
        PreparedStatement pstmt = connection.prepareStatement(sql);
        ResultSet rs = pstmt.executeQuery();

        List<Customer> list = new ArrayList<>();

        while(rs.next()) {
        	Customer row = new Customer();
            row.setcustomer_id(rs.getInt("customer_id"));
            row.setname(rs.getString("name"));      
            list.add(row);
        }
        return list;
    }

    @Override
    public void save(Customer model) throws SQLException {
        String sql = "INSERT INTO customers (customer_id, name) VALUES (?,?)";
        PreparedStatement pstmt = connection.prepareStatement(sql);
        pstmt.setInt(1, model.getcustomer_id());
        pstmt.setString(2, model.getname());

        if(pstmt.executeUpdate() > 0) {
            pstmt.getResultSet();
        }
    }

    @Override
    public void update(Customer model, String[] params) throws SQLException {
    	String sql = "UPDATE customer SET name = ? WHERE customer_id = ? ";
    	PreparedStatement pstmt = connection.prepareStatement(sql);
    	pstmt.setString(1, model.getname()); 
    	pstmt.setInt(2, model.getcustomer_id());
      

    }

    @Override
    public void delete(Customer model) throws SQLException {
    	String sql = "DELETE customer WHERE customer_id = ? ";
    	PreparedStatement pstmt = connection.prepareStatement(sql);
    	pstmt.setInt(1, model.getcustomer_id());	

    }
}
