package daos;

import pacakgesexceptions.NoSQLResultsException;
import models.Account;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

public class AccountDAO implements Dao<Account>{
    //private List<TestTable> testTables;
    Connection connection;

    public AccountDAO(Connection conn) {
        //testTables = new ArrayList<>();
        connection = conn;
    }

    @Override
    public Account get(int id) throws SQLException, NoSQLResultsException {
        String sql = "Select * FROM accounts WHERE account_id = ? LIMIT 1";
        PreparedStatement pstmt = connection.prepareStatement(sql);
        pstmt.setInt(1, id);

        ResultSet rs = pstmt.executeQuery();
        if(rs.next()) {
        	Account row = new Account();
            row.setaccount_id(rs.getInt("account_id"));
            row.setbalance(rs.getFloat("balance"));
            return row;
        } else {
            throw new NoSQLResultsException ("ID: " + id + " not found.");
        }
    }

    @Override
    public List<Account> getAll() throws SQLException {
        String sql = "SELECT * FROM accounts";
        PreparedStatement pstmt = connection.prepareStatement(sql);
        ResultSet rs = pstmt.executeQuery();

        List<Account> list = new ArrayList<>();

        while(rs.next()) {
        	Account row = new Account();
            row.setaccount_id(rs.getInt("account_id"));
            row.setbalance(rs.getFloat("balance"));      
            list.add(row);
        }
        return list;
    }

    @Override
    public void save(Account model) throws SQLException {
        String sql = "INSERT INTO accounts (account_id, balance) VALUES (?,?)";
        PreparedStatement pstmt = connection.prepareStatement(sql);
        pstmt.setInt(1, model.getaccount_id());
        pstmt.setFloat(2, model.getbalance());

        if(pstmt.executeUpdate() > 0) {
            pstmt.getResultSet();
        }
    }

    @Override
    public void update(Account model, String[] params) throws SQLException {
    	String sql = "UPDATE customer SET name = ? WHERE customer_id = ? ";
    	PreparedStatement pstmt = connection.prepareStatement(sql);
    	pstmt.setInt(1, model.getaccount_id()); 
    	pstmt.setFloat(2, model.getbalance());
      

    }

    @Override
    public void delete(Account model) throws SQLException {
    	String sql = "DELETE account WHERE account_id = ? ";
    	PreparedStatement pstmt = connection.prepareStatement(sql);
    	pstmt.setInt(1, model.getaccount_id());	

    }
}
