package daos;

import pacakgesexceptions.NoSQLResultsException;
import models.AccountCustomer;
import org.mariadb.jdbc.internal.util.dao.PrepareResult;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.List;

public class AccountCustomerDAO<AccountsCustomers> implements Dao<AccountCustomer>{
    //private List<TestTable> testTables;
    Connection connection;

    public AccountCustomerDAO(Connection conn) {
        //testTables = new ArrayList<>();
        connection = conn;
    }

    @Override
    public AccountCustomer get(int id) throws SQLException, NoSQLResultsException {
        String sql = "Select * FROM accounts_customers WHERE account_id = ? LIMIT 1";
        PreparedStatement pstmt = connection.prepareStatement(sql);
        pstmt.setInt(1, id);

        ResultSet rs = pstmt.executeQuery();
        if(rs.next()) {
            AccountCustomer row = new AccountCustomer();
            row.setaccount_id(rs.getInt("account_id"));
            row.setname(rs.getString("name"));
            row.setaccount_id(rs.getInt("account_id"));
            row.setbalance(rs.getFloat("balance"));
            return row;
        } else {
            throw new NoSQLResultsException ("ID: " + id + " not found.");
        }
    }

    @Override
    public List<AccountCustomer> getAll() throws SQLException {
        String sql = "SELECT * FROM accounts_customers";
        PreparedStatement pstmt = connection.prepareStatement(sql);
        ResultSet rs = pstmt.executeQuery();

        List<AccountCustomer> accountcustomerList = new ArrayList<>();

        while(rs.next()) {
            AccountCustomer row = new AccountCustomer();
            row.setaccount_id(rs.getInt("account_id"));
            row.setname(rs.getString("name"));
            row.setcustomer_id(rs.getInt("customer_id"));
            row.setbalance(rs.getFloat("balance"));
            accountcustomerList.add(row);
        }
        return accountcustomerList;
    }

    @Override
    public void save(AccountCustomer account_customer) throws SQLException {
        String sql = "INSERT INTO accounts_customers (int) VALUES (?)";
        PreparedStatement pstmt = connection.prepareStatement(sql);
        pstmt.setInt(1, account_customer.getcustomer_id());

        if(pstmt.executeUpdate() > 0) {
            pstmt.getResultSet();
        }
    }

    @Override
    public void update(AccountCustomer account_customer, String[] params) throws SQLException {
    	String sql = "UPDATE accounts_customers SET balance WHERE account_id = ? ";
    	PreparedStatement pstmt = connection.prepareStatement(sql);
    	pstmt.setFloat(1, account_customer.getbalance());

    }

    @Override
    public void delete(AccountCustomer account_customer) {

    }
}
