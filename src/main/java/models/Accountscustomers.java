package models;



public class AccountCustomer {
	private int account_id;
	private int customer_id;
	private float balance;
	private String name;
	
	
	public AccountCustomer() {
		super();
	}
	
	public AccountCustomer(int account_id, int customer_id,  float balance, String name) { //make name parameter
		super();
		this.account_id = account_id;
		this.customer_id = customer_id;
		this.balance = balance;
		
	}
	public int getaccount_id() {
		return account_id;
	}
	public void setaccount_id(int account_id) {
		this.account_id = account_id;
	}
	public int getcustomer_id() {
		return customer_id;
	}
	public void setcustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public void setbalance(float balance) {
		this.balance = balance;
	}
	public void setname(String name) {
		this.name = name;
	}
	

	
	@Override
	public String toString() {
		return "AccountCustomer [account_id=" + account_id + ", customerID=" + customer_id + ",balance=" + balance + ",name=" + name +"]";
	}

	public void setStringId(int int1) {
		// TODO Auto-generated method stub
		
	}

	public void setString(String string) {
		// TODO Auto-generated method stub
		
	}

	public float getbalance() {
		// TODO Auto-generated method stub
		return 0;
	}

	
}
