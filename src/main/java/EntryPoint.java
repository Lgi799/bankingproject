//import controllers.NonStaticController;
//import controllers.TestController;

import io.javalin.Javalin;
import models.Customer;
import models.AccountCustomer;
import models.Account;
import util.ConnectionFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import controllers.AccountController;
import controllers.CustomerController;
import controllers.TestController;
import controllers.AccountCustomerController;

public class EntryPoint {
    public static void main(String[] args) {

        Javalin app = Javalin.create().start(7000);
        Connection conn = ConnectionFactory.getConnection();
       AccountCustomerController accountcustomercontroller = new AccountCustomerController(app, conn);
       TestController testController = new TestController(app, conn);
       CustomerController customerController = new CustomerController(app,conn);
       AccountController accountController = new AccountController(app,conn);
     
      


    }
} 

