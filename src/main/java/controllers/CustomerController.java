package controllers;

import daos.CustomerDAO;
import pacakgesexceptions.NoSQLResultsException;
import io.javalin.Javalin;
import io.javalin.http.Context;
import models.Customer;
import util.ConnectionFactory;

import java.sql.SQLException;
import java.util.List;
import java.sql.Connection;
public class CustomerController {
	
	private Javalin app; 
	private Connection conn;
	
	public CustomerController(Javalin app, Connection conn) {
		this.app = app;
		this.conn = conn;
		app.get("/customers_test", this::testconnection);
		app.get("/customers", this::getAll);
		app.post("/customers", this::insertNewData);
		app.get("/customers/:id", this::getById);
	}
	
	//private static Javalin javalin;
	
	public  void testconnection(Context ctx) {
		ctx.status(200);
		ctx.result("Test customer");
		
	}
	
	public  void insertNewData(Context ctx) throws SQLException{
		CustomerDAO dao = new CustomerDAO (ConnectionFactory.getConnection());
		Customer row = ctx.bodyAsClass(Customer.class);
		dao.save(row);
		ctx.status(201);
	}

	
  public  void getById(Context ctx) throws SQLException, NoSQLResultsException {
	  CustomerDAO  dao = new CustomerDAO (ConnectionFactory.getConnection());
        Customer row = dao.get(Integer.parseInt(ctx.pathParam("id")));
        ctx.json(row);
    }
  public  void getAll(Context ctx) throws SQLException, NoSQLResultsException {
	  CustomerDAO  dao = new CustomerDAO (ConnectionFactory.getConnection());
	  List<Customer> rows = dao.getAll();
	  
        //Customer row = dao.get(Integer.parseInt(ctx.pathParam("id")));
        ctx.json(rows);
    }
	
}



