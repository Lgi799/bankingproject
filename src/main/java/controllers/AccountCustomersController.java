package controllers;

import daos.AccountCustomerDAO;
import daos.CustomerDAO;
import pacakgesexceptions.NoSQLResultsException;
import io.javalin.Javalin;
import io.javalin.http.Context;
import models.AccountCustomer;
import models.Customer;
import util.ConnectionFactory;

import java.sql.SQLException;
import java.util.List;
import java.sql.Connection;
public class AccountCustomerController{
	
	private Javalin app; 
	private Connection conn;
	
	public AccountCustomerController(Javalin app, Connection conn) {
		this.app = app;
		this.conn = conn;
		app.get("/accountcustomer_test", this::testconnection);
		app.get("/accountcustomer", this::getAll);
		app.post("/accountcustomer", this::insertNewData);
		app.get("/accountcustomer/:id", this::getById);
	}
	
	//private static Javalin javalin;
	
	public  void testconnection(Context ctx) {
		ctx.status(200);
		ctx.result("Test account_customer");
		
	}
	
	public  void insertNewData(Context ctx) throws SQLException{
		AccountCustomerDAO dao = new AccountCustomerDAO (ConnectionFactory.getConnection());
		AccountCustomer row = ctx.bodyAsClass(AccountCustomer.class);
		dao.save(row);
		ctx.status(201);
	}

	
  public  void getById(Context ctx) throws SQLException, NoSQLResultsException {
	  AccountCustomerDAO  dao = new AccountCustomerDAO (ConnectionFactory.getConnection());
        AccountCustomer row = dao.get(Integer.parseInt(ctx.pathParam("id")));
        ctx.json(row);
    }
  public  void getAll(Context ctx) throws SQLException, NoSQLResultsException {
	  AccountCustomerDAO  dao = new AccountCustomerDAO (ConnectionFactory.getConnection());
	  List<AccountCustomer> rows = dao.getAll();
	  
        //Customer row = dao.get(Integer.parseInt(ctx.pathParam("id")));
        ctx.json(rows);
    }
	
}



